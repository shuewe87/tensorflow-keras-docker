#!/bin/bash

setOptions(){
echo ""
echo "Start reading options for tensorflow-keras-image"
export JUPYTER_LOGPATH="/tensorflow/log_jupyter"
ADDITIONS=()
 while true; do
        case "${1}" in
            --jupyter )
                JUPYTER="true"
                echo "Jupyter notebook option found.. Will start it at the end of the setup process."
                 TO_EXEC="bash"
                shift ;;
            --jupyter-logpath )
                JUPYTER_LOGPATH="$2"
                echo "Jupyter logpath set to $2."
                shift 2 ;;
            "")
                break ;;
            * )
                ADDITIONS+=("$1")
                TO_EXEC=${ADDITIONS[@]}
                shift ;;
        esac
 done

echo "Path to execute: $TO_EXEC"
echo "reading options finished"
echo ""

}



##
#Main script
##

setOptions "$@"

#Check if folder was mounted
python /git-folder-scripts/makeFolderFromJson.py  /setup/tensorflow-mount.json

#Look if wheele should be downloaded
bash /scripts/checkForTensorflowDownload.sh

#Get filename of wheele (from download or direct mounted)
wheele=$(python /scripts/getTensorflowWheele.py)

#Install wheele if one was given
if [ ! -z "$wheele" ]; then
 echo "Wheele file for tensorflow found.."
 pip install --upgrade $wheele
fi 

if [ ! -z $JUPYTER ]; then	
 bash /scripts/startJupyter.sh &> $JUPYTER_LOGPATH &
fi

bash /git-folder-scripts/start.sh ${TO_EXEC[@]}

