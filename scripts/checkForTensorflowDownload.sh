#!/bin/bash

if [ -e /tensorflow/ ]; then
  if [ -f /tensorflow/downloadurl ]; then
   url=$( cat /tensorflow/downloadurl )
   path=$( pwd )
   cd /tensorflow
   if [ -n "$url" ]; then
    wget $url
   fi
   cd "$path"
  fi
fi
