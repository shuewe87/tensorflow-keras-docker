#!/bin/bash


if [ ! -e /jupyter ]; then
 mkdir /jupyter
fi
cd /jupyter
jupyter-notebook --ip="0.0.0.0" --no-browser --allow-root 


